import numpy as np
import pandas as pd

st_gallen_id = '3203'
file_names_ages = [
    '../data/age_0-14_percent.csv',
    '../data/age_15-19_percent.csv',
    '../data/age_20-39_percent.csv',
    '../data/age_40-64_percent.csv',
    '../data/age_65-79_percent.csv',
    '../data/age_80plus_percent.csv'
]

ages_data_size = 12


def read_data(path):
    df = pd.read_csv(path, delimiter=',', parse_dates=[0], thousands="'")
    return df


def regression(x, y):
    [beta_1, beta_0] = np.polyfit(x, y, deg=1)
    return lambda m: m * beta_1 + beta_0


def get_data(file_name, data_size):
    data = read_data(file_name)
    total = data[(data['BFS_NR'] == st_gallen_id)].iloc[:, 2: data_size + 2].sort_index(axis=1)
    x = [int(v) for v in total.columns.values]
    y = total.iloc[0].array
    return [x, y]


def transform_to_absolute(y):
    [_, y_population] = get_data("../data/population.csv", ages_data_size)

    y_multiplied = [0] * ages_data_size

    for i in range(0, ages_data_size):
        y_multiplied[i] = y[i] * y_population[i] / 100

    return y_multiplied


def predict(file_name, absolute=False):
    [x, y] = get_data(file_name, ages_data_size)
    if absolute:
        y = transform_to_absolute(y)
    return [x, y, regression(x, y)]
