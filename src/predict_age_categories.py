import matplotlib.pyplot as plt
import numpy as np

from helpers import file_names_ages, predict


def plot(file_name):
    [x, y, f] = predict(file_name, True)
    plt.bar(x, y)

    model_line = np.linspace(2010, 2025)
    plt.plot(model_line, f(model_line), 'r')

    prediction_X = range(2022, 2026)
    prediction_Y = []

    for x in prediction_X:
        prediction_Y.append(f(x))

    plt.bar(prediction_X, prediction_Y)

    plt.title(file_name)
    plt.show()


for file_name in file_names_ages:
    plot(file_name)
