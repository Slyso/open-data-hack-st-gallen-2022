import matplotlib.pyplot as plt

from helpers import get_data, file_names_ages, ages_data_size, transform_to_absolute


def plot_absolute_age_history():
    current_sum_array = [0] * ages_data_size
    for file_name in file_names_ages:
        [x, y] = get_data(file_name, ages_data_size)

        y_multiplied = transform_to_absolute(y)

        plt.bar(x, y_multiplied, bottom=current_sum_array)

        for i in range(0, ages_data_size):
            current_sum_array[i] += y_multiplied[i]

    plt.show()


plot_absolute_age_history()
