import matplotlib.pyplot as plt

from helpers import get_data


def plot_population_history(file_name):
    [x, y] = get_data(file_name, 40)
    plt.bar(x, y)

    # model_line = np.linspace(1981, 2030)
    # plt.plot(model_line, regression(x, y)(model_line), 'r')

    plt.show()


plot_population_history("../data/population.csv")
