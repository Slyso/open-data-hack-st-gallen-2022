import matplotlib.pyplot as plt

from helpers import get_data, file_names_ages, ages_data_size, predict

x_captions = [
    '0-14 y',
    '15-19 y',
    '20-39 y',
    '40-64 y',
    '65-79 y',
    '80+ y',
]

datasets = []
y_relative_base = []
x = []

prediction_count = 5

for file_name in file_names_ages:
    [x, y] = get_data(file_name, ages_data_size)
    y = y.tolist()
    datasets.append(y)
    y_relative_base.append(y[0])

    [_, _, f] = predict(file_name)
    prediction_xs = range(2022, 2022 + prediction_count)
    prediction_ys = []

    for prediction_x in prediction_xs:
        x.append(prediction_x)
        y.append(f(prediction_x))


def plot():
    for i in range(ages_data_size + prediction_count):
        for j in range(len(datasets)):
            y = datasets[j][i]
            base = y_relative_base[j]
            plt.ylim([0.5, 1.5])
            plt.bar(x_captions[j], y / base)
            plt.title(x[i])
            if x[i] > 2022:
                plt.title(str(x[i]) + " (predicted)")
        plt.show()


plot()
