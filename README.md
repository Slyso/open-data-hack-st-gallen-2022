## Introduction

This is a submission for the Open Data Hack St. Gallen 2022 (see https://opendatahack-stgallen.devpost.com/). In this submission, we evaluate, discuss, and analyze the
demography of the city of St. Gallen. Background for this work is the ongoing shortage of skilled
workers in the IT department.

## Diagrams

### Population History

This is the population history for St. Gallen, and our starting point for our further analysis. This
and all further data is openly available (see https://stada2.sg.ch/). In this diagram, we can see
that the population for the city of St. Gallen is currently growing.

![Absolute Age](doc/population_history.png)

### Absolute Age

Here is the population history, colored by age (of St. Gallen).

![Absolute Age](doc/absolute_age_history.png)

![Blue](doc/icons/blue.png) ages 0-14 |
![Orange](doc/icons/orange.png) ages 15-19 |
![Green](doc/icons/green.png) ages 20-39 |
![Red](doc/icons/red.png) ages 40-64 |
![Purple](doc/icons/purple.png) ages 65-79 |
![Brown](doc/icons/brown.png) ages 80+

### Trend and Prediction

These are the population histories and predictions by age (of St. Gallen).

![Prediction](doc/prediction_0-14.png)
![Prediction](doc/prediction_15-19.png)
![Prediction](doc/prediction_20-39.png)
![Prediction](doc/prediction_40-64.png)
![Prediction](doc/prediction_65-79.png)
![Prediction](doc/prediction_80_plus.png)

### Demographic trend animated

In the following animated image we display the different demographic layers. Each image depicts the
demographic layers of one specific year, compared to the last year.

![Demographic trend animated](doc/demographic_trend/animation.gif)

## Conclusion

As we can see in the diagrams above, the population of the working age group is growing slightly.
However, it is not growing fast enough compared to the current needs of the market.

There is a silver lining: the population of the ages 0-14 is growing very fast, so the shortage problem should be reduced a
bit in within the next 10-20 years.

## Limitations

The population by age is only available for years starting from 2010 and onwards. Therefore, we
couldn't include older data.

## Source of data

- https://www.stadt.sg.ch/home/verwaltung-politik/stadt-zahlen/statistiken-finden.html
- https://stada2.sg.ch/
